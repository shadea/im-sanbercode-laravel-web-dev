<?php 

    require_once('Animal.php');
    require_once('Frog.php');
    require_once('Ape.php');

    $animal = new Animal("Shaun");

    echo "Nama : " . $animal->nama . "<br>";
    echo "Jumlah kaki : " . $animal->kaki . "<br>";
    echo "Cold-Blooded : " . $animal->cold . "<br><br>";    


    $frog = new Frog("Buduk");

    echo "Nama : " . $frog->nama . "<br>";
    echo "jumlah kaki : " . $frog->kaki . "<br>";
    echo "cold-blooded : " . $frog->cold . "<br>";
    
    echo $frog-> berjalan("Hop Hop") . "<br>"."<br>";

    $kera = new Ape("Kera");

    echo "Nama : " . $kera->nama . "<br>";
    echo "jumlah kaki : " . $kera->kaki . "<br>";
    echo "cold-blooded : " . $kera->cold . "<br>";
    
    echo $kera-> berjalan("Auooo") . "<br>";
?>