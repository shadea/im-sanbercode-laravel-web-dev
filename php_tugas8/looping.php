<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Soal Looping</h1>
    <?php 
    echo "<h3>Contoh 1</h3>";

    $x = 2;

    echo "<h4>Looping</h4>";
    do{
    echo "$x - I Love PHP <br>";
    $x+=2;
    } while ($x <= 20);

    echo "<h4>looping 2</h4>";

    for ($a=20; $a >= 1 ; $a-=2){
        echo "$a - I Love PHP <br>";
    }

    echo "<h3>Contoh 2</h3>";

    $nomor = [24, 35, 20, 67, 43];

    echo "array numbers : ";
    print_r($nomor);
    
    foreach ($nomor as $value){

        $rest[] = $value %= 5;

    }

    echo "<br>";
    echo "array sisa baginya adalah: ";
    print_r($rest);
    echo "<br>";

    echo "<h3>Contoh 3</h3>";

    $trainer = [

        // ["Rezky", 26, "Laravel"],
        // ["Thio", 30, "Phyton"],
        // ["Yoga", 25, "ReactJS"],
        ['001', 'Keyboard Logitek', 60000, 'Keyboard yang mantap untuk kantoran', 'logitek.jpeg'], 
        ['002', 'Keyboard MSI', 300000, 'Keyboard gaming MSI mekanik', 'msi.jpeg'],
        ['003', 'Mouse Genius', 50000, 'Mouse Genius biar lebih pinter', 'genius.jpeg'],
        ['004', 'Mouse Jerry', 30000, 'Mouse yang disukai kucing', 'jerry.jpeg']
    ];

    foreach($trainer as $val)
    {
        $tampung = [
            'id' => $val[0],
            'name' => $val[1],
            'price' => $val[2],
            'description' => $val[3],
            'source' => $val[4]


        ];
        print_r($tampung);
        echo "<br>";
    }

    echo "<h3>Contoh 4</h3>";

    for($i=1; $i<=5; $i++){
        for($r=1; $r<=$i; $r++){
            echo "*";
        }
        echo "<br>";
    }
    ?>
</body>
</html>