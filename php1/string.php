
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
        echo "<h3> Soal No 1</h3>";
        $kalimat1 = "halo semua";
        echo "Kalimat 1 = " . $kalimat1 . "<br>";
        echo "Kalimat Panjang String = " . strlen($kalimat1) . "<br>";
        /* 
            SOAL NO 1
            Tunjukan dengan menggunakan echo berapa panjang dari string yang diberikan berikut! Tunjukkan juga jumlah kata di dalam kalimat tersebut! 

            Contoh: 
            $string = "PHP is never old";
            Output:
            Panjang string: 16, 
            Jumlah kata: 4 
        */

        $first_sentence = "Hello PHP!" ; // Panjang string 10, jumlah kata: 2
        $second_sentence = "I'm ready for the challenges"; // Panjang string: 28,  jumlah kata: 5
        
        echo "<h3> Soal No 2</h3>";
        $kalimat2 = "I love PHP";
        echo "Kalimat 2 = " . $kalimat2 . "<br>";
        echo "Kata 1 " . substr($kalimat2,0,1) . "<br>";
        echo "Kata 2 " . substr($kalimat2,2,4) . "<br>";        
        echo "Kata 3 " . substr($kalimat2,7,3) . "<br>";       
        
        /* 
            SOAL NO 2
            Mengambil kata pada string dan karakter-karakter yang ada di dalamnya. 
            
            
        */


        echo "<h3> Soal No 3 </h3>";
        $kalimat3 = "PHP is old but sexy!";
        echo "kalimat 3 = " . $kalimat3 . "<br>";
        echo "ubah kalimat 3 = " . str_replace("sexy!","awesome",$kalimat3) . "<br>";

        /*
            SOAL NO 3
            Mengubah karakter atau kata yang ada di dalam sebuah string.
        */
       
        // OUTPUT : "PHP is old but awesome"

    ?>
</body>
</html>